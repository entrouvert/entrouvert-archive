.PHONY: clean name version name fullname release dist dist-bzip2
all: output/entrouvert-archive.gpg

NAME = entrouvert-archive
VERSION=`git describe | sed 's/^v//; s/-/./g'`

DIST_FILES = \
	keys \
	bullseye \
	bookworm \
	Makefile

output/entrouvert-archive.gpg: keys/0x*
	cat keys/0x* > output/entrouvert-archive.gpg

clean:
	rm -rf sdist
	rm -rf output/*
	test -d output || mkdir output

release:
	gbp dch --release --commit
	gbp buildpackage --git-tag

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

dist-bzip2: dist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
